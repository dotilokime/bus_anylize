import grequests

import json
import os
import pathlib

url = "http://127.0.0.1:8000"

files_directory = pathlib.Path("C:\\Projects\\fmon-2")

files_paths = [os.path.join(files_directory, file_name) for file_name in os.listdir(files_directory) if file_name.endswith(".json")]


points_list = []

for file_path in files_paths:
    try:
        with open(file_path, "r", encoding="utf-8") as f:
            data = json.load(f)
            points = data["anims"]

            points_list.append(points)
    except Exception:
        print(file_path)

responses = (grequests.post(f"{url}/points/external/bulk", json=points) for points in points_list)
grequests.map(responses, size=10)
