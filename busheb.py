import math
import time

from requests import session

session = session()

headers = {
    "Cookie": "PHPSESSID=89rje788akq710b5bgkhdr7u11",
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:97.0) Gecko/20100101 Firefox/97.0"
    }

url = "http://buscheb.ru/php/getVehiclesMarkers.php?rids=355-0,356-0,10000-1,299-0,284-0,311-0,1-0,3-0,50-0,49-0,353-0,354-0,362-0,309-0,310-0,347-0,346-0,53-0,54-0,307-0,11-0,358-0,360-0,379-0,378-0,363-0,16-0,364-0,17-0,328-0,329-0,20-0,373-0,59-0,371-0,23-0,343-0,26-0,46-0,45-0,300-0,29-0,375-0,344-0,32-0,337-0,338-0,297-0,36-0,35-0,67-0,68-0,37-0,316-0,305-0,348-0,281-0,345-0,76-0,77-0,78-0,79-0,331-0,104-0,332-0,334-0,121-0,120-0,320-0,319-0,99-0,98-0,83-0,82-0,122-0,123-0,95-0,94-0,100-0,101-0,114-0,115-0,124-0,125-0,80-0,81-0,102-0,103-0,87-0,335-0,333-0,86-0,96-0,148-0,150-0,365-0,366-0,376-0,377-0,367-0,368-0,381-0,380-0,383-0,382-0,384-0,385-0,387-0,386-0,389-0,388-0,127-0,126-0,282-0,283-0&lat0=0&lng0=0&lat1=90&lng1=180&curk=0&city=cheboksari&info=12345&_=1645240652202"
while True:

    try:

        timestamp = math.trunc(time.time())
        response = session.get(url, headers=headers, timeout=15)
        assert response.status_code == 200

        filename = f"./json/{timestamp}.json"

        with open(filename, "w", encoding="utf-8") as f:
            f.write(response.text)
            print(f"Записан файл {filename}")

    except AssertionError as e:
        print(e)

    except Exception as e:
        print(e)

    time.sleep(5)

