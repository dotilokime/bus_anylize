import os

import numpy as np
import pandas as pd

rynok = (81085690, 64153083)
grazgdanskay = (81017619, 64160110)


# считать датафреймы
# объединить фреймы
def load_csv(files_path):
    names = ["id", "lon", "lat", "dir", "speed", "lasttime", "gos_num", "rid", "rnum", "rtype", "low_floor", "wifi", "anim_key", "big_jump"]
    frames = []
    for file in files_path:
        d = pd.read_csv(file, encoding="utf-8", header=1, names=names, sep=";")
        frames.append(d)
    df = pd.concat(frames)
    df["lasttime"] = pd.to_datetime(df["lasttime"], format="%d.%m.%Y %H:%M:%S", utc=True)
    df = df.set_index(pd.DatetimeIndex(df["lasttime"]))
    df.sort_index(inplace=True)
    return df


# выбрать один день из фрейма
def select_day(df, day):
    return df.loc[day, :]
    pass


def select_route(df, ts_type, route_number):
    return df[(df["rtype"] == ts_type) & (df["rnum"] == route_number)]


def get_distance(lon_1, lat_1, lon_2, lat_2):
    return abs(((lon_1 - lon_2) ** 2 + (lat_1 - lat_2) ** 2) ** (1/2))


def find_intervals(df, start_coordinates, stop_coordinates):
    numbers = df["gos_num"].unique()
    for number in numbers:
        d = df[df["gos_num"] == number]
        d['row_num'] = np.arange(len(d))
        d["start_distance"] = d.apply(lambda row: abs(((row["lon"] - start_coordinates[0]) ** 2 + (row["lat"] - start_coordinates[1]) ** 2) ** (1/2)), axis=1)
        d["stop_distance"] = d.apply(lambda row: abs(((row["lon"] - stop_coordinates[0]) ** 2 + (row["lat"] - stop_coordinates[1]) ** 2) ** (1/2)), axis=1)
        print(d["start_distance"].head())
        print(d[d['start_distance'] < 10000]["row_num"].tolist())
        print(d["stop_distance"].head())
    pass


def mean_intervals(df):
    pass


def plot(df):
    pass


csv_path = "C:\\buscheb_data\\csv\\"

files = [os.path.join(csv_path, name) for name in os.listdir(csv_path)]
df = load_csv(files)

df = select_day(df, "2022-02-18")

df = select_route(df, "Т", 14)

find_intervals(df, rynok, grazgdanskay)

print(df.head())
print(df["gos_num"].unique())



# найти остановки (начало и конец интервала)
# составить фрейм интервалов
# усреднить
# отобразить график
